# README #

### What is this repository for? ###

PasswordManager v1.1 is simple program with GUI for storing passwords and only for storing localy with small, but fast sqlite 3.8.7 database. Database file is encrypted, so you can store safely whatever password you want to store.

Input is website, username and password.

[DOWNLOAD PAGE](https://bitbucket.org/felix557700/passwordmanagergui/downloads)

##TODO: 
1. Encrypt only data (password), not whole file. Maybe someone could intersect encryption and deletion of raw database file.
2. Securely store encryption key (not hardcoded).
3. Support backup in case of deletion of database file.
/**
 * @author Filip Vitas
 * @version 1.1
 */

import javax.swing.UIManager;

public class PasswordManagerGUI 
{
    public static void main(String[] args) 
    {
    	try 
    	{
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel"); //javax.swing.plaf.nimbus.NimbusLookAndFeel
		} 
    	catch (Exception e) { e.printStackTrace(); }
    	
        PasswordManagerFrame frame = new PasswordManagerFrame();
        frame.setVisible(true);
    }    
}

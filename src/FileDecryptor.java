/**
 * Simple modification of "FileEncrypor.java".  The
 * previously encrypted "clear.txt.des" is read, decrypted
 * and written back as "clear.txt.dcr"
 * 
 * @author Ray Watkins
 * Professor in the Mathematics department at Saddleback College, Mission Viejo, CA
 */

import java.io.*;
import java.security.*;

import javax.crypto.*;
import javax.crypto.spec.*;

import java.util.*;

public class FileDecryptor
{
   private static String filename;
   private static String password;
   private static FileInputStream inFile;
   private static FileOutputStream outFile;

   public static void decrypt(String name) throws Exception
   {
      // File to decrypt.

      filename = name;

      String password = "super_secret_password";

      inFile = new FileInputStream(filename + ".db");
      outFile = new FileOutputStream(filename);

      PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray());
      SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
      SecretKey passwordKey = keyFactory.generateSecret(keySpec);

      // Read in the previouly stored salt and set the iteration count.

      byte[] salt = new byte[8];
      inFile.read(salt);
      int iterations = 100;

      PBEParameterSpec parameterSpec = new PBEParameterSpec(salt, iterations);

      // Create the cipher and initialize it for decryption.

      Cipher cipher = Cipher.getInstance("PBEWithMD5AndDES");
      cipher.init(Cipher.DECRYPT_MODE, passwordKey, parameterSpec);


      byte[] input = new byte[64];
      int bytesRead;
      while ((bytesRead = inFile.read(input)) != -1)
      {
    	  byte[] output = cipher.update(input, 0, bytesRead);
    	  if (output != null)
            outFile.write(output);
      }

      byte[] output = cipher.doFinal();
      if (output != null)
         outFile.write(output);

      inFile.close();
      outFile.flush();
      outFile.close();
      
      // delete temp(encrypted) database
      File file = new File(name + ".db");
      file.delete();
  }
}